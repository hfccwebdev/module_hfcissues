<?php

/**
 * @file
 * This module adds issue tracking functionality to comment form.
 *
 * @ingroup hfcc_modules
 */

/**
 * Implements hook_entity_info().
 */
function hfcissues_entity_info() {
  return array(
    'hfcissue' => array(
      'label' => t('Issue Changelog'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'hfcissues_changelog',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'cid',
      ),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'module' => 'hfcissues',
      'static cache' => TRUE,
    ),
  );
}

/**
 * Implements hook_entity_property_info_alter().
 */
function hfcissues_entity_property_info_alter(&$info){
  $properties = &$info['hfcissue']['properties'];

  $properties['cid'] = array(
    'label' => t("Comment ID"),
    'type' => 'comment',
    'description' => t("The unique ID of the comment."),
    'schema field' => 'cid',
  );

  $properties['nid'] = array(
    'label' => t("Node ID"),
    'type' => 'node',
    'description' => t("The unique ID of the node."),
    'schema field' => 'nid',
  );

  $properties['uid'] = array(
    'label' => t("User ID"),
    'type' => 'user',
    'description' => t("The unique ID of the comment author."),
    'schema field' => 'uid',
  );

  $properties['issue_before'] = array(
    'label' => t("Node data before"),
    'type' => 'struct',
    'description' => t("The issue state before this comment."),
    'schema field' => 'issue_before',
  );

  $properties['issue_after'] = array(
    'label' => t("Node data after"),
    'type' => 'struct',
    'description' => t("The issue state after this comment."),
    'schema field' => 'issue_after',
  );

}

/**
 * Implements hook_form_alter().
 *
 * This is needed for hook_form_BASE_FORM_ID_alter() to work. It's dumb.
 */
function hfcissues_form_alter(&$form, &$form_state, $form_id) {
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function hfcissues_form_issue_node_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form_state['node']->nid) && $form_state['node']->nid) {
    $form['title']['#disabled'] = TRUE;
    $form['field_assigned']['#disabled'] = TRUE;
    $form['field_urgency']['#disabled'] = TRUE;
    $form['field_system']['#disabled'] = TRUE;
    $form['field_category']['#disabled'] = TRUE;
    $form['field_status']['#disabled'] = TRUE;
    drupal_set_message(t('Please post a comment to update issue status information.'), 'warning');
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function hfcissues_form_comment_node_issue_form_alter(&$form, &$form_state, $form_id) {
  // Only add issue fields on new comments!
  if (!empty($form['cid']['#value'])) {
    return;
  }

  $node = $form['#node'];
  $wrapper = entity_metadata_wrapper('node', $node);

  $form['hfcissues'] = array(
    '#type' => 'fieldset',
    '#title' => t('Edit issue settings'),
    '#description' => t("Note: changing any of these items will update the issue's overall values."),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#weight' => -1,
  );

  $form['hfcissues']['issue_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Issue title'),
    '#default_value' => $node->title,
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $field_data = field_info_field('field_assigned');

  $form['hfcissues']['issue_assigned'] = array(
    '#type' => 'select',
    '#title' => 'Assigned',
    '#options' => array('0' => 'Unassign') + _options_get_options($field_data, NULL, array('strip_tags_and_unescape' => TRUE)),
    '#default_value' => $wrapper->field_assigned->raw(),
    '#required' => FALSE,
  );

  $field_data = field_info_field('field_urgency');

  $form['hfcissues']['issue_urgency'] = array(
    '#type' => 'select',
    '#title' => t('Urgency'),
    '#options' => $field_data['settings']['allowed_values'],
    '#default_value' => $wrapper->field_urgency->value(),
    '#required' => TRUE,
  );

  $field_data = field_info_field('field_system');

  $form['hfcissues']['issue_system'] = array(
    '#type' => 'select',
    '#title' => t('System'),
    '#options' => $field_data['settings']['allowed_values'],
    '#default_value' => $wrapper->field_system->value(),
    '#required' => TRUE,
  );

  $field_data = field_info_field('field_category');

  $form['hfcissues']['issue_category'] = array(
    '#type' => 'select',
    '#title' => t('Category'),
    '#options' => $field_data['settings']['allowed_values'],
    '#default_value' => $wrapper->field_category->value(),
    '#required' => TRUE,
  );

  $field_data = field_info_field('field_status');

  $form['hfcissues']['issue_status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => $field_data['settings']['allowed_values'],
    '#default_value' => $wrapper->field_status->value(),
    '#required' => TRUE,
  );

  $form['#submit'][] = 'hfcissues_comment_form_submit';
}

/**
 * Custom comment form submission handler.
 */
function hfcissues_comment_form_submit($form, &$form_state) {
  // Only act if we've been sent issue data.
  if (!isset($form_state['values']['hfcissues'])) {
    return;
  }

  $values = $form_state['values']['hfcissues'];

  $hfcissue = entity_create('hfcissue', array(
    'cid' => $form_state['values']['cid'],
    'nid' => $form_state['values']['nid'],
  ));

  if (!$form_state['values']['is_anonymous']['#value'] && !empty($form_state['values']['name']) && $account = user_load_by_name($form_state['values']['name'])) {
    $hfcissue->uid = $account->uid;
  }
  else {
    $hfcissue->uid = 0;
  }

  $node = node_load($form_state['values']['nid']);
  $wrapper = entity_metadata_wrapper('node', $node);

  $hfcissue->issue_before = _hfcissues_issue_values($wrapper);

  $changed = FALSE;

  if ($values['issue_title'] !== $wrapper->label()) {
    $wrapper->title = $values['issue_title'];
    $changed = TRUE;
  }

  if ($values['issue_assigned'] !== $wrapper->field_assigned->raw()) {
    $wrapper->field_assigned = $values['issue_assigned'];
    $changed = TRUE;
  }

  if ($values['issue_urgency'] !== $wrapper->field_urgency->raw()) {
    $wrapper->field_urgency = $values['issue_urgency'];
    $changed = TRUE;
  }

  if ($values['issue_system'] !== $wrapper->field_system->raw()) {
    $wrapper->field_system = $values['issue_system'];
    $changed = TRUE;
  }

  if ($values['issue_category'] !== $wrapper->field_category->raw()) {
    $wrapper->field_category = $values['issue_category'];
    $changed = TRUE;
  }

  if ($values['issue_status'] !== $wrapper->field_status->raw()) {
    $wrapper->field_status = $values['issue_status'];
    $changed = TRUE;
  }

  if ($changed) {
    node_save($node);
  }

  $hfcissue->issue_after = _hfcissues_issue_values($wrapper);
  entity_save('hfcissue', $hfcissue);
}

/**
 * Build array of issue values to store.
 */
function _hfcissues_issue_values($wrapper) {
  return array(
    'issue_title' => $wrapper->label(),
    'issue_assigned' => $wrapper->field_assigned->value(),
    'issue_urgency' => $wrapper->field_urgency->value(),
    'issue_system' => $wrapper->field_system->value(),
    'issue_category' => $wrapper->field_category->value(),
    'issue_status' => $wrapper->field_status->value(),
  );
}
